<?php

return [
    'class' => [
        'parent' => env('INOMENU_PARENT', 'list-unstyled components'),
        'item' => env('INOMENU_ITEM', ''),
        'active' => env('INOMENU_ACTIVE', 'active'),
        'toggle_handler' => env('INOMENU_TOGGLE_HANDLER', 'dropdown-toggle'),
        'toggled' => env('INOMENU_TOGGLED_ITEM', 'collapse list-unstyled'),
    ],
    'role_key_type' => 'unsignedInteger',
];
