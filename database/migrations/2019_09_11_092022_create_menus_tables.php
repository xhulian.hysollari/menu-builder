<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMenusTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $roleIdKeyType = config('menubuilder.role_key_type');
        Schema::create('menus', function (Blueprint $table) use ($roleIdKeyType) {
            $table->increments('id');
            $table->$roleIdKeyType('role_id')->index();
            $table->string('menu_key');
            $table->longText('structure');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menus');
    }
}
