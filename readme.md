# MenuBuilder

[![Latest Version on Packagist][ico-version]][link-packagist]
[![Total Downloads][ico-downloads]][link-downloads]
[![Build Status][ico-travis]][link-travis]
[![StyleCI][ico-styleci]][link-styleci]

This is where your description should go. Take a look at [contributing.md](contributing.md) to see a to do list.

## Installation

@todolist
make the instalation procedure

## Usage

## Change log

Please see the [changelog](changelog.md) for more information on what has changed recently.

## Testing

```bash
$ composer test
```

## Contributing

Please see [contributing.md](contributing.md) for details and a todolist.

## Security

If you discover any security related issues, please email xhulian.hysollari@gmail.com instead of using the issue tracker.

## Credits

-   [xhulian hysollari][link-author]
-   [All Contributors][link-contributors]

## License

MIT. Please see the [license file](license.md) for more information.

[ico-version]: https://img.shields.io/packagist/v/inovacion/menubuilder.svg?style=flat-square
[ico-downloads]: https://img.shields.io/packagist/dt/inovacion/menubuilder.svg?style=flat-square
[ico-travis]: https://img.shields.io/travis/inovacion/menubuilder/master.svg?style=flat-square
[ico-styleci]: https://styleci.io/repos/12345678/shield
[link-packagist]: https://packagist.org/packages/inovacion/menubuilder
[link-downloads]: https://packagist.org/packages/inovacion/menubuilder
[link-travis]: https://travis-ci.org/inovacion/menubuilder
[link-styleci]: https://styleci.io/repos/12345678
[link-author]: https://github.com/inovacion
[link-contributors]: ../../contributors
