<?php

namespace Inovacion\MenuBuilder\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use Inovacion\MenuBuilder\MenuBuilder;
use Inovacion\MenuBuilder\Models\Menu;

class MenuController extends Controller
{
    public function index()
    {
        if ($menu = Cache::get('menu.menu_keys')) {
            return response()->json($menu, 200);
        }
        $menu = Cache::rememberForever('menu.menu_keys', function () {
            return Menu::distinct('menu_key')->orderBy('id')->pluck('menu_key');
        });

        return response()->json($menu, 200);
    }

    public function getActiveMenu($key, $roleId)
    {
        $menu = (new MenuBuilder())->getMenuByKey($key, $roleId);

        return response()->json($menu, 200);
    }
}
