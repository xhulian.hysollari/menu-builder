<?php

namespace Inovacion\MenuBuilder\Console;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Cache;
use Inovacion\MenuBuilder\MenuBuilder;
use Inovacion\MenuBuilder\Models\Menu;

class MenuGeneratorCommand extends Command
{
    protected static $MENU_ITEMS = [
        'basic',
        'start',
        'actual',
        'end',
        'report',
    ];
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'inovacion:menu:generate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generates the user menu';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $this->clearCache();

        return (new MenuBuilder())->initBuilder();
    }

    public function clearCache()
    {
        foreach (self::$MENU_ITEMS as $item) {
            for ($i = 1; $i <= 10; $i++) {
                Cache::forget("menu.$item.$i");
            }
        }
        Cache::forget('menu.menu_keys');
        Menu::truncate();
    }
}
