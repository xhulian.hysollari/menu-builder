<?php

namespace Inovacion\MenuBuilder;

use Illuminate\Support\ServiceProvider;

class MenuBuilderServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(__DIR__ . '/../resources/lang', 'inovacion');
        
        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');
        $this->app['router']->namespace('Inovacion\\MenuBuilder\\Controllers')
            ->prefix('api')
            ->middleware(['auth:api', 'navigation'])
            ->group(function () {
                $this->loadRoutesFrom(__DIR__ . '/routes.php');
            });

        // Publishing is only necessary when using the CLI.
        $this->bootForConsole();
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/menubuilder.php', 'menubuilder');

        // Register the service the package provides.
        $this->app->singleton('menubuilder', function ($app) {
            return new MenuBuilder();
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['menubuilder'];
    }

    /**
     * Console-specific booting.
     *
     * @return void
     */
    protected function bootForConsole()
    {
        // Publishing the configuration file.
        $this->publishes([
            __DIR__ . '/../config/menubuilder.php' => config_path('menubuilder.php'),
        ], 'menubuilder.config');

        // Publishing vuejs files.
        $this->publishes([
            __DIR__ . '/../resources/js/components' => base_path('resources/js/components/menu'),
        ], 'menu-components');

        // Registering package commands.
        $this->commands([
            Console\MenuGeneratorCommand::class,
        ]);
    }
}
