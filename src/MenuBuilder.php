<?php

namespace Inovacion\MenuBuilder;

use App\Models\Role;
use Illuminate\Support\Facades\Cache;
use Inovacion\MenuBuilder\Models\Menu;


//@ToDo: This is where the menu structure is created, this is built to better fit the needs of the application so changes might be needed to make it as a 100% standalone package

class MenuBuilder
{
    protected $reports = [
        'student_per_class',
        'foreign_students',
        'reports_school',
        'reports_registrations',
        'reports_registrations_per_class',
        'reports_registrations_per_class_per_type',
        'reports_registrations_per_age',
        'reports_classrooms_per_school',
        'reports_classrooms_per_orientation',
        'reports_foreign_registrations_per_class',
        'reports_employees',
        'reports_job_positions',
        'reports_employee_age_brackets',
        'reports_employee_exp_brackets',
        'reports_teacher_personal_qualifications',
        'reports_minority_registrations',
        'reports_special_need_registrations',
        'reports_collective_registrations',
        'reports_registrations_from_e_albania',
        'reports_logs',
        'student_transfer_reports',
        'reports_dormitories',
        'reports_suburban_students',
        'reports_high_school_graduates',
        'reports_graduates_per_orientation',
        'reports_teacher_trainings',
        'reports_investment_needs',
        'reports_recruitment_needs',
    ];

    protected $exclude = [
        'change_selected_school_info',
    ];

    public function initBuilder()
    {
        $data = json_decode(file_get_contents(__DIR__ . '/menu.json'));
        return $this->buildTree($data);
    }

    public function buildTree($data)
    {
        foreach ($data as $key => $menu) {
            $this->generateMenuByRole($key,$menu);
        }
        return 1;
    }

    public function generateMenuByRole($key,$menu)
    {
        foreach (Role::all() as $role) {
            $generatedMenu = $this->generateMenuByKey($key,$menu,$role);
            Menu::create([
                'role_id' => $role->id,
                'menu_key' => $key,
                'structure' => json_encode($generatedMenu),
            ]);
        }
    }

    private function generateMenuByKey($key,$menu,$role)
    {
        $permissions = $role->permissions()->pluck('type','name')->toArray();
        $generatedMenu = [];
        foreach ($menu as $key2 => $item) {
            if ($key2 !== 'home') {
                if (in_array($key2,$this->reports)) {
                    if ($role->hasPermissionTo('manage_reports')) {
                        $report['icon'] = $item->icon;
                        $report['is_an'] = isset($item->is_an) ? $item->is_an : true;
                        $report['is_ap'] = isset($item->is_ap) ? $item->is_ap : true;
                        $report['text'] = $item->text;
                        $report['link'] = $item->link;
                        if ($role->type == 2 && ($report['is_ap'] === true)) {
                            $generatedMenu[$key2] = $report;
                        } elseif ($role->type == 1 && ($report['is_an'] === true)) {
                            $generatedMenu[$key2] = $report;
                        } elseif ($role->type == 0 && ($report['is_an'] === true || $report['is_ap'] === true)) {
                            $generatedMenu[$key2] = $report;
                        }
                    }
                } else {
                    $parent = [];
                    if (isset($item->is_parent) && $item->is_parent) {
                        $parent['link'] = $item->link;
                        $parent['icon'] = $item->icon;
                        $parent['text'] = $item->text;
                        if ($role->type == 2) {
                            $parent['text'] = isset($item->text_ap) ? $item->text_ap : $item->text;
                        }
                        $parent['children'] = [];
                        $itemChildren = collect($item->children)->sortBy('text');
                        foreach ($itemChildren as $key3 => $child) {
                            isset($child->is_an) ?: $child->is_an = true;
                            isset($child->is_ap) ?: $child->is_ap = true;
                            if (in_array($key3,array_keys($permissions)) || in_array($key3,$this->exclude)) {
                                if (($role->type == 2 || $permissions[$key3] == 2) && $child->is_ap) {
                                    $currentMenu['link'] = "/ap$child->link";
                                } else {
                                    $currentMenu['link'] = $child->link;
                                }
                                if ($role->type == 2 && $child->is_ap && isset($child->text_ap)) {
                                    $currentMenu['text'] = $child->text_ap;
                                } elseif ($role->type == 1 && $child->is_an && isset($child->text_an)) {
                                    $currentMenu['text'] = $child->text_an;
                                } else {
                                    $currentMenu['text'] = $child->text;
                                }
                                $currentMenu['icon'] = $child->icon;
                                $currentMenu['nativeLink'] = isset($child->nativeLink) ? $child->nativeLink : false;
                                $parent['children'][$key3] = $currentMenu;
                            }
                        }
                        if (count($parent['children'])) {
                            $generatedMenu[$key2] = $parent;
                        }
                    }
                }
            } else {
                $currentMenu['text'] = $item->text;
                $currentMenu['link'] = $item->link;
                $currentMenu['icon'] = $item->icon;
                $generatedMenu[$key2] = $currentMenu;
            }
        }

        return $generatedMenu;
    }

    public function getMenuByKey($key,$roleId)
    {
        if ($menu = Cache::get("menu.$key.$roleId")) {
            return $menu->structure;
        }
        $menu = Cache::rememberForever("menu.$key.$roleId",function () use ($key,$roleId){
            return Menu::where('menu_key',$key)->where('role_id',$roleId)->first();
        });

        return $menu->structure;
    }
}
