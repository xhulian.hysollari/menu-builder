<?php

Route::get('current-menu/{key}/{roleId}', 'MenuController@getActiveMenu');
Route::get('menus', 'MenuController@index');
